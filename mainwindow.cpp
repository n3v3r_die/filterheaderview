#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtSql>
#include <QTableView>
#include <QSqlRecord>
#include "filterheaderview.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , messageBox(new QErrorMessage(this))
{
    ui->setupUi(this);
    createDataBase();
    setupTableView();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_filterTextChanged(const int column, const QString filterText)
{
    const QString fieldName = model->record().fieldName(column);

    if (filterText.isEmpty()){
        model->setFilter("");
    }else{
        model->setFilter(QString("%0 like '%1%'").arg(fieldName, filterText));
    }
    if (!model->select())
        messageBox->showMessage(model->lastError().text());
}

void MainWindow::createDataBase()
{
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(":memory:");
    if (!db.open())
        messageBox->showMessage(db.lastError().text());

    QSqlQuery query;
    if (!query.exec("create table test_table("
                    "id integer primary key,"
                    "name text,"
                    "surname text,"
                    "email text"
                    ");")){
        messageBox->showMessage(query.lastError().text());
        return;
    }
    query.prepare("insert into test_table values (?, ?, ?, ?)");
    QVariantList ints;
    ints << 1 << 2 << 3 << 4 << 5;
    query.addBindValue(ints);

    QVariantList names;
    names << "Nikolay" << "Harald" << "Boris" << "Trond" << "Niklaus";
    query.addBindValue(names);


    QVariantList surnames;
    surnames << "Porohin" << "Potter" << "Godunov" << "Wiskeas" << "Pegegree";
    query.addBindValue(surnames);

    QVariantList emails;
    emails << "porohin@sqllite.com" << "potter@sqllite.com"
          << "godunov@sqllite.com" << "wiskeas@sqllite.com" << "pegegree@sqllite.com";
    query.addBindValue(emails);

    if (!query.execBatch())
        messageBox->showMessage(query.lastError().text());
}

void MainWindow::setupTableView()
{
    QTableView *tableView = new QTableView(this);
    model = new QSqlTableModel(this);
    model->setTable("test_table");
    if (!model->select()){
        messageBox->showMessage(model->lastError().text());
        return;
    }
    tableView->setModel(model);
    centralWidget()->layout()->addWidget(tableView);
    auto headerView = new FilterHeaderView(this);
    tableView->setSortingEnabled(true);
    tableView->setHorizontalHeader(headerView);
    tableView->setHorizontalScrollMode(QAbstractItemView::ScrollMode::ScrollPerPixel);
    connect(headerView, &FilterHeaderView::filterHViewTextChanged, this, &MainWindow::on_filterTextChanged);
}

