#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QErrorMessage>

class QSqlTableModel;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void on_filterTextChanged(const int column, const QString filterText);
private:
    Ui::MainWindow *ui;
    QSqlTableModel * model;
    QErrorMessage *messageBox;
    void createDataBase();
    void setupTableView();
};
#endif // MAINWINDOW_H
