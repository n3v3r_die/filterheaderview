# FilterHeaderView

Custom QHeaderView with filter for QTableView.

A simple Qt widget for adding a filter input field under the title.

`class FilterHeaderView: public QHeaderView`

Creates `QLineEdit` as filter input field and emit signal `void filterHViewTextChanged(const int Column, const QString text)` when button "Enter" was clicked.

![Preview](https://gitlab.com/n3v3r_die/filterheaderview/-/blob/master/preview.png?raw=true "Preview")
