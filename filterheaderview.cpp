#include "filterheaderview.h"
#include <QLineEdit>
#include <QScrollBar>


FilterHeaderView::FilterHeaderView(QWidget *parent) :
    QHeaderView(Qt::Horizontal, parent),
    oldOffset(offset())
{
    setSectionsMovable(true);
    setSectionsClickable(true);
    connect(this, &FilterHeaderView::sectionMoved, this, &FilterHeaderView::adjustPositions);
    connect(this, &FilterHeaderView::sectionResized, this, &FilterHeaderView::adjustPositions);
}

void FilterHeaderView::setModel(QAbstractItemModel *model)
{
    QHeaderView::setModel(model);
    clearFilterWidgets();
    createFilterWidgets(model->columnCount());
}

QSize FilterHeaderView::sizeHint() const
{

    QSize size = QHeaderView::sizeHint();
    if(m_lineEditFiters.size() > 0)
    {
        const int height = m_lineEditFiters[0]->sizeHint().height();
        size.setHeight(size.height() + height);
    }
    return size;
}

void FilterHeaderView::paintEvent(QPaintEvent *event)
{
    QHeaderView::paintEvent(event);
    if (oldOffset != offset())
        adjustPositions();
}

void FilterHeaderView::updateGeometries()
{
    if(m_lineEditFiters.size() > 0)
    {
        const int height = m_lineEditFiters[0]->sizeHint().height();
        setViewportMargins(0, 0, 0, height);
    }
    else{
        setViewportMargins(0, 0, 0, 0);
    }
    QHeaderView::updateGeometries();
    adjustPositions();
}

void FilterHeaderView::adjustPositions()
{
    oldOffset = offset();
    for (size_t i = 0; i < m_lineEditFiters.size(); i++)
    {
        int sectionIndex = static_cast<int>(i);
        auto lineEdit = m_lineEditFiters[i];
        lineEdit->move(sectionPosition(sectionIndex) - offset(),
                       size().height() - lineEdit->sizeHint().height());
        lineEdit->resize(sectionSize(sectionIndex), lineEdit->sizeHint().height());
    }
}

void FilterHeaderView::clearFilterWidgets()
{
    while(!m_lineEditFiters.empty()){
        auto lineEdit = m_lineEditFiters.back();
        delete lineEdit;
        m_lineEditFiters.pop_back();
    }
}

void FilterHeaderView::createFilterWidgets(int columnCount)
{
    for (int i = 0; i < columnCount; i++){
        auto lineEdit = new QLineEdit(this);
        lineEdit->setStyleSheet(QString("QLineEdit {border: 1px solid #bbb; padding: 3px;}"));
        lineEdit->setPlaceholderText(tr("Filter"));
        connect(lineEdit, &QLineEdit::returnPressed, [this, i]
                {
                    emit filterHViewTextChanged(i, m_lineEditFiters[static_cast<size_t>(i)]->text());
                });
        m_lineEditFiters.push_back(lineEdit);
    }
}
