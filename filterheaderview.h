#ifndef FILTERHEADERVIEW_H
#define FILTERHEADERVIEW_H
#include <QHeaderView>

class FilterHeaderView : public QHeaderView
{
    Q_OBJECT
public:
    explicit FilterHeaderView (QWidget *parent = nullptr);
    virtual void setModel(QAbstractItemModel *model);
    QSize sizeHint() const;

protected:
    void paintEvent(QPaintEvent *event);
protected slots:
    virtual void updateGeometries();
private:
    int oldOffset;
    std::vector<QLineEdit*> m_lineEditFiters;
    void adjustPositions();
    void clearFilterWidgets();
    void createFilterWidgets(int columnCount);

signals:
    void filterHViewTextChanged(const int column, const QString text);
};

#endif // FILTERHEADERVIEW_H
